package Main;

/**
 * Created by Csicsószki Zsombor on 3/20/2017.
 */

import Main.PageObject.CalendarPage;
import Main.PageObject.LoginPage;
import Main.Utils.WebDriverManager;
import org.openqa.selenium.WebDriver;

public class Main {

  public static void main(String[] args) {

    WebDriver driver = WebDriverManager.getDriver();

    LoginPage loginPage = new LoginPage(driver);
    CalendarPage calendarPage = new CalendarPage(driver);

    loginPage.login();

    calendarPage.clickCreateButton().typeEventName("Meeting")
        .typeFromDate("3/24/2017").typeFromTime("16:30")
        .typeUntilDate("3/24/2017").typeUntilTime("18:00")
        .clickSaveButton();

    calendarPage.logOut();

    WebDriverManager.closeDriver();
  }
}


package Main.Utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Created by Csicsószki Zsombor on 3/21/2017.
 */
public class WebDriverManager {

  private static WebDriver driver;
  private static final String DRIVER_PATH = "C://Program Files (x86)//GoogleWebDriver//chromedriver.exe";

  /**
   * Setting driver executable
   * Method to create pageobject
   * Null check to make sure
   */
  public static WebDriver getDriver() {
    System.setProperty("webdriver.chrome.driver", DRIVER_PATH);
    if (driver == null)
      driver = new ChromeDriver();
    return driver;
  }

  /**
   * This method will close the driver
   */
  public static void closeDriver(){
    driver.close();
    driver.quit();
  }
}

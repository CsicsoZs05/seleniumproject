package Main.Utils;

import org.openqa.selenium.WebDriver;

/**
 * Created by Csicsószki Zsombor on 3/23/2017.
 */
public class CookieUtils {

  private WebDriver driver;

  public CookieUtils(WebDriver driver) {
    this.driver = driver;
  }

  /**
   * This method clears/deletes all cookies, this way we can logout
   */
  public void deleteAllCookies() {
    driver.manage().deleteAllCookies();
  }
}

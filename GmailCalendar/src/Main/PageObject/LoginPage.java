package Main.PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Csicsószki Zsombor on 3/21/2017.
 */
public class LoginPage extends BasePageObject {

  private static final String USER_NAME = "csicsozs05.test@gmail.com";
  private static final String PASSWORD = "csicsoszki2017";
  private static final String GOOGLE_CALENDAR_URL = "https://www.google.com/calendar";

  @FindBy(id = "Email")
  private WebElement userNameField;
  @FindBy(id = "Passwd")
  private WebElement passwordField;
  @FindBy(id = "next")
  private WebElement nextButton;
  @FindBy(id = "signIn")
  private WebElement signInButton;

  private WebDriver driver;

  public LoginPage(WebDriver driver) {
    super(driver);
    this.driver = driver;
  }

  /**
   * Navigates to Google Calendar
   * @return
   */
  public LoginPage navigateTo() {
    driver.get(GOOGLE_CALENDAR_URL);
    return this;
  }

  /**
   * Finds and enters the username
   * @return
   */
  public LoginPage typeUsername() {
    userNameField.sendKeys(USER_NAME);
    return this;
  }

  /**
   * Clicks on the Next Button
   * @return
   */
  public LoginPage clickNextButton() {
    nextButton.click();
    return this;
  }

  /**
   * Finds and enters the password
   * @return
   */
  public LoginPage typePassword() {
    passwordField.sendKeys(PASSWORD);
    return this;
  }

  /**
   * Clicks on the Sign in button to login
   * @return
   */
  public LoginPage clickSignInButton() {
    signInButton.click();
    return this;
  }

  /**
   * This method handles the other methods
   */
  public void login() {
    navigateTo().typeUsername().clickNextButton().typePassword().clickSignInButton();
  }
}

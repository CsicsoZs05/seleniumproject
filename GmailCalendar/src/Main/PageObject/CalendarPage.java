package Main.PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Csicsószki Zsombor on 3/21/2017.
 */

public class CalendarPage extends BasePageObject {

  @FindBy(css = "#createEventButtonContainer > div > div.goog-imageless-button-collapse-right")
  private WebElement createButton;
  @FindBy(css = "div.ep-title > input[title='Event title'][type='text']")
  private WebElement typeEvent;
  @FindBy(xpath = "//*[@title='From date']")
  private WebElement fromDate;
  @FindBy(xpath = "//*[@title='From time']")
  private WebElement fromTime;
  @FindBy(xpath = "//*[@title='Until time']")
  private WebElement untilTime;
  @FindBy(xpath = "//*[@title='Until date']")
  private WebElement untilDate;
  @FindBy(css = "div.action-btn-wrapper.ep-ea-btn-wrapper > div[role = 'button'][tabindex = '0']")
  private WebElement saveButton;

  private WebDriver driver;

  public CalendarPage(WebDriver driver) {
    super(driver);
    this.driver = driver;
  }

  /**
   * Clicks on the create button to schedule a meeting
   * @return
   */
  public CalendarPage clickCreateButton() {
    createButton.click();
    return this;
  }

  /**
   * Enters a name for events
   * @param event
   * @return
   */
  public CalendarPage typeEventName(String event) {
    typeEvent.sendKeys(event);
    return this;
  }

  /**
   * Sets the start date of the meeting
   * @param date
   * @return
   */
  public CalendarPage typeFromDate(String date) {
    fromDate.clear();
    fromDate.sendKeys(date);
    return this;
  }

  /**
   * Sets the start time of the meeting
   * @param time
   * @return
   */
  public CalendarPage typeFromTime(String time) {
    fromTime.clear();
    fromTime.sendKeys(time);
    return this;
  }

  /**
   * Sets the end time of the meeting
   * @param time
   * @return
   */
  public CalendarPage typeUntilTime(String time) {
    untilTime.clear();
    untilTime.sendKeys(time);
    return this;
  }

  /**
   * Sets the end date of the meeting
   * @param date
   * @return
   */
  public CalendarPage typeUntilDate(String date) {
    untilDate.clear();
    untilDate.sendKeys(date);
    return this;
  }

  /**
   * Clicks on the save button
   * @return
   */
  public CalendarPage clickSaveButton() {
    saveButton.click();
    return this;
  }
}

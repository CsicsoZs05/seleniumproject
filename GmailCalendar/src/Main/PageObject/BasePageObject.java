package Main.PageObject;

import Main.Utils.CookieUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

/**
 * Created by Csicsószki Zsombor on 3/21/2017.
 */
public class BasePageObject {

  private static final int WAIT_INTERVAL_SECONDS = 20;
  private WebDriver driver;
  private CookieUtils cookieUtils;

  public BasePageObject(WebDriver driver) {
    PageFactory.initElements(new AjaxElementLocatorFactory(driver, WAIT_INTERVAL_SECONDS), this);
    this.driver = driver;
    cookieUtils = new CookieUtils(driver);
  }

  /**
   * This method deletes all the cookies, therefore logs out
   */
  public void logOut() {
    cookieUtils.deleteAllCookies();
  }
}
